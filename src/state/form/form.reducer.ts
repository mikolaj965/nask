import { DEFAULT_CURRENCY_FROM, DEFAULT_CURRENCY_TO } from '../../constants/config';
import { FormChangeEnum, calculateConversionResult, onFormChange, setDefaultFormCurrencyTypes, switchDirection } from './form-utils';

import { IConverterFormModel } from '../../app/models/converterForm.model';
import { ICurrencyDetailsAPIModel } from '../../app/models/currencyDetailsAPI.model';

export type IFormState = Readonly<typeof initialState>;

export const mapReducer = (dispatch: any) => ({
    setFormValue: (type: FormChangeEnum, value: any) => dispatch(setFormValue(type, value)),
    setDefaultFormTypes: (cl: ICurrencyDetailsAPIModel[]) => dispatch(setDefaultFormTypes(cl)),
    switchConversionDirection: () => dispatch(switchConversionDirection()),
    calculateResult: (conversion: number) => dispatch(calculateResult(conversion))
});

export const initialState = {
    data: {
        from: {
            value: 1.00,
            type: ""
        },
        to: {
            value: null,
            type: ""
        }
    } as IConverterFormModel
};

const PREFIX = "FORM";

const ACTIONS = {
    ON_CHANGE: `${PREFIX}/ON_CHANGE`,
    CALCULATE_RESULT: `${PREFIX}/CALCULATE_RESULT`,
    SWITCH_DIRECTION: `${PREFIX}/SWITCH_DIRECTION`,
    SET_DEFAULT_TYPES: `${PREFIX}/SET_DEFAULT_TYPES`
}

export default (state: IFormState = initialState, action: any): IFormState => {
    const { type, payload } = action;
    switch (type) {
        case ACTIONS.ON_CHANGE:
            return {
                ...state,
                data: onFormChange(state.data, payload.type, payload.value)
            };
        case ACTIONS.CALCULATE_RESULT:
            return {
                ...state,
                data: calculateConversionResult(state.data, payload.data)
            };
        case ACTIONS.SWITCH_DIRECTION:
            return {
                ...state,
                data: switchDirection(state.data)
            };
        case ACTIONS.SET_DEFAULT_TYPES:
            return {
                ...state,
                data: setDefaultFormCurrencyTypes(state.data, payload.data, DEFAULT_CURRENCY_FROM, DEFAULT_CURRENCY_TO)
            };
        default:
            return state;
    }
};

const setFormValue = (type: FormChangeEnum, value: any) => ({
    type: ACTIONS.ON_CHANGE,
    payload: {
        type,
        value
    }
});

const calculateResult = (conversion: number) => ({
    type: ACTIONS.CALCULATE_RESULT,
    payload: {
        data: conversion
    }
});

const switchConversionDirection = () => ({
    type: ACTIONS.SWITCH_DIRECTION
});

const setDefaultFormTypes = (currencyList: ICurrencyDetailsAPIModel[]) => ({
    type: ACTIONS.SET_DEFAULT_TYPES,
    payload: {
        data: currencyList
    }
});
