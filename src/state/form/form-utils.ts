import { IConverterFormModel } from '../../app/models/converterForm.model';
import { ICurrencyDetailsAPIModel } from '../../app/models/currencyDetailsAPI.model';

export enum FormChangeEnum {
    MONEY_FROM,
    CURRENCY_FROM,
    CURRENCY_TO
}

const onFormChange = (current: IConverterFormModel, type: FormChangeEnum, value: any): IConverterFormModel => {
    const tmp = { ...current };
    switch (type) {
        case FormChangeEnum.MONEY_FROM:
            tmp.from.value = value;
            break;
        case FormChangeEnum.CURRENCY_FROM:
            tmp.from.type = value;
            break;
        case FormChangeEnum.CURRENCY_TO:
            tmp.to.type = value;
            break;
    }
    tmp.to.value = null;
    return tmp;
}

const calculateConversionResult = (current: IConverterFormModel, conversion: number): IConverterFormModel => {
    const tmp = { ...current };
    if (conversion && !isNaN(conversion) && tmp.from.value !== null) {
        tmp.to.value = +((conversion * tmp.from.value).toFixed(2));
    }
    return tmp;
}

const switchDirection = (current: IConverterFormModel): IConverterFormModel => {
    const tmp = JSON.parse(JSON.stringify(current));
    const oldToType = tmp.to.type;
    tmp.to.type = tmp.from.type;
    tmp.from.type = oldToType;
    tmp.to.value = null;
    return tmp;
}

const setDefaultFormCurrencyTypes = (current: IConverterFormModel, data: ICurrencyDetailsAPIModel[], from: string, to: string): IConverterFormModel => {
    const tmp = { ...current };
    const currencyList = data ? data.map(x => x.id) : [];
    tmp.from.type = currencyList.includes(from) ? from : '';
    tmp.to.type = currencyList.includes(to) ? to : '';
    return tmp;
}

export {
    onFormChange,
    calculateConversionResult,
    switchDirection,
    setDefaultFormCurrencyTypes
};