import { API_ALL_CURRENCIES, API_CURRENT_CONVERSION } from '../../constants/api';
import { ERROR, PENDING, SUCCESS } from '../../utils/action-utils';
import { addHistoryDataAndSaveLocalStorage, clearHistoryLocalStorage, loadHistoryFromLocalStorage } from './currency-utils';

import { API_SECRET } from '../../constants/config';
import { IConversionHistoryModel } from '../../app/models/conversionHistory.model';
import { IConverterFormModel } from '../../app/models/converterForm.model';
import { ICurrencyDetailsAPIModel } from '../../app/models/currencyDetailsAPI.model';
import axios from 'axios';

export type ICurrencyState = Readonly<typeof initialState>;

export const initialState = {
    all: [] as ICurrencyDetailsAPIModel[],
    history: loadHistoryFromLocalStorage() as IConversionHistoryModel[],
    apiInProgress: false
}

const PREFIX = "CURRENCIES";

const ACTIONS = {
    FETCH_ALL_CURRENCIES: `${PREFIX}/FETCH_ALL_CURRENCIES`,
    FETCH_CURRENT_CONVERSION: `${PREFIX}/FETCH_CURRENT_CONVERSION`,
    ADD_HISTORY_DATA: `${PREFIX}/ADD_HISTORY_DATA`,
    CLEAR_HISTORY_DATA: `${PREFIX}/CLEAR_HISTORY_DATA`
}

export const mapReducer = (dispatch: any) => ({
    fetchAll: () => fetchAllCurrencies()(dispatch),
    fetchConversion: (conversion: string) => fetchConversion(conversion)(dispatch),
    clearHistoryData: () => dispatch(clearHistoryData()),
    addHistoryData: (data: IConverterFormModel) => dispatch(addHistoryData(data))
});

export default (state: ICurrencyState = initialState, action: any): ICurrencyState => {
    const { type, payload } = action;
    switch (type) {
        case PENDING(ACTIONS.FETCH_ALL_CURRENCIES):
            return {
                ...state,
                all: [],
                apiInProgress: true
            };
        case ERROR(ACTIONS.FETCH_ALL_CURRENCIES):
            return {
                ...state,
                all: [],
                apiInProgress: false
            };
        case SUCCESS(ACTIONS.FETCH_ALL_CURRENCIES):
            return {
                ...state,
                all: payload.data && payload.data.results ? Object.values(payload.data.results) : [],
                apiInProgress: false
            };

        case PENDING(ACTIONS.FETCH_CURRENT_CONVERSION):
            return {
                ...state,
                apiInProgress: true
            };
        case ERROR(ACTIONS.FETCH_CURRENT_CONVERSION):
            return {
                ...state,
                apiInProgress: false
            };
        case SUCCESS(ACTIONS.FETCH_CURRENT_CONVERSION):
            return {
                ...state,
                apiInProgress: false
            };
        case ACTIONS.ADD_HISTORY_DATA:
            return {
                ...state,
                history: addHistoryDataAndSaveLocalStorage(state.history, payload.data)
            };
        case ACTIONS.CLEAR_HISTORY_DATA:
            clearHistoryLocalStorage();
            return {
                ...state,
                history: []
            }
        default:
            return state;
    }
};


const fetchAllCurrencies = () => (dispatch: any) => {
    dispatch({
        type: PENDING(ACTIONS.FETCH_ALL_CURRENCIES)
    });

    return axios.get<ICurrencyDetailsAPIModel[]>(`${API_ALL_CURRENCIES}?apiKey=${API_SECRET}`)
        .then(res => dispatch({
            type: SUCCESS(ACTIONS.FETCH_ALL_CURRENCIES),
            payload: {
                data: res.data
            }
        }))
        .catch(err => dispatch({
            type: ERROR(ACTIONS.FETCH_ALL_CURRENCIES),
            error: err
        }));
}

const fetchConversion = (conversion: string) => (dispatch: any): Promise<number> => {
    dispatch({
        type: PENDING(ACTIONS.FETCH_CURRENT_CONVERSION)
    });

    return axios.get<any>(`${API_CURRENT_CONVERSION}&q=${conversion}&apiKey=${API_SECRET}`)
        .then(res => {
            dispatch({
                type: SUCCESS(ACTIONS.FETCH_CURRENT_CONVERSION),
                payload: {
                    data: res.data
                }
            });
            return Number(Object.values(res.data)[0]);
        })
        .catch(err => {
            dispatch({
                type: ERROR(ACTIONS.FETCH_CURRENT_CONVERSION),
                error: err
            });
            return err;
        });
}

const addHistoryData = (data: IConverterFormModel) => ({
    type: ACTIONS.ADD_HISTORY_DATA,
    payload: {
        data
    }
});

const clearHistoryData = () => ({
    type: ACTIONS.CLEAR_HISTORY_DATA
});
