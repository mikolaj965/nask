import { HISTORY_LOCAL_STORAGE_NAME } from '../../constants/config';
import { IConversionHistoryModel } from '../../app/models/conversionHistory.model';
import { IConverterFormModel } from '../../app/models/converterForm.model';
import cloneDeep from 'lodash/cloneDeep';
import moment from 'moment';

const loadHistoryFromLocalStorage = (): IConversionHistoryModel[] => {
    const history = localStorage.getItem(HISTORY_LOCAL_STORAGE_NAME);
    if (history) {
        var historyObj = JSON.parse(history) as IConversionHistoryModel[];
        historyObj.forEach(h => h.date = moment(h.date));
        return historyObj;
    }
    return [];
}

const addHistoryDataAndSaveLocalStorage = (current: IConversionHistoryModel[], newRecord: IConverterFormModel): IConversionHistoryModel[] => {
    const tmp = [...current];
    const _newRecord = cloneDeep(newRecord);
    _newRecord.from.value = Number(_newRecord.from.value);
    _newRecord.to.value = Number(_newRecord.to.value);
    tmp.push({
        id: Math.random().toString(36).substring(7),
        date: moment(),
        before: _newRecord.from,
        after: _newRecord.to
    });
    localStorage.setItem(HISTORY_LOCAL_STORAGE_NAME, JSON.stringify(tmp));
    return tmp;
}

const clearHistoryLocalStorage = () => {
    localStorage.setItem(HISTORY_LOCAL_STORAGE_NAME, JSON.stringify([]));
}

export {
    addHistoryDataAndSaveLocalStorage,
    clearHistoryLocalStorage,
    loadHistoryFromLocalStorage
}