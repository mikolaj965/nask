import { API_ERROR_DEATILS } from './constants/config';
import { toast } from 'react-toastify';

export default {
    onRequestFulfilled: (response: any) => response,
    onRequestRejected: (error: any) => {
        toast.error(API_ERROR_DEATILS);
        if (error?.response?.data) {
            return Promise.reject(error.response.data);
        }
        return Promise.reject(error.message);
    }
}
