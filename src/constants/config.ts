export const API_SECRET = "91f4aef229fe5e0c583d";
export const HISTORY_LOCAL_STORAGE_NAME = "history";
export const DEFAULT_CURRENCY_FROM = "PLN";
export const DEFAULT_CURRENCY_TO = "USD";
export const API_ERROR_TITLE = "Komunikat błędu";
export const API_ERROR_DEATILS = "Nie udało się wykonać żądanej operacji, ponieważ nie znaleziono zasobu powiązanego z żądaniem.";