
export const API_HOST = "https://free.currconv.com/api";
export const API_VERSION = "v7";
export const API_ALL_CURRENCIES = `${API_HOST}/${API_VERSION}/currencies`;
export const API_CURRENT_CONVERSION = `${API_HOST}/${API_VERSION}/convert?compact=ultra`;