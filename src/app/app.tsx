import './styles/app.scss';

import React, { FC, useEffect, useMemo, useReducer } from 'react';
import currencyReducer, { initialState as currencyInitialState, mapReducer as mapCurrencyReducer } from './../state/currency/currency.reducer';
import formReducer, { initialState as formInitialState, mapReducer as mapFormReducer } from '../state/form/form.reducer';

import Converter from './scenes/components/converter/converter';
import { FormChangeEnum } from '../state/form/form-utils';
import LoadingWrapper from './scenes/components/loadingWrapper/loadingWrapper';
import { useHistory } from 'react-router-dom';

interface IAppProps {
  historyOpened: boolean;
}

const App: FC<IAppProps> = props => {
  const { historyOpened } = props;

  const history = useHistory();

  const [{
    all: currencies,
    apiInProgress: isApiInProgress,
    history: historyData
  }, currencyDispatch] = useReducer(currencyReducer, currencyInitialState);
  const [{
    data: formData,
  }, formDispatch] = useReducer(formReducer, formInitialState);

  const currencyActions = useMemo(() => mapCurrencyReducer(currencyDispatch), [currencyDispatch]);
  const formActions = useMemo(() => mapFormReducer(formDispatch), [formDispatch]);
  const conversion = useMemo(() => `${formData.from.type}_${formData.to.type}`, [formData.to.type, formData.from.type]);

  // EFFECTS
  useEffect(() => {
    currencyActions.fetchAll();
  }, [currencyActions]);

  useEffect(() => {
    formActions.setDefaultFormTypes(currencies);
  }, [formActions, currencies])

  // ACTIONS 
  const handleConvertAction = () => {
    currencyActions.fetchConversion(conversion)
      .then((conversion: number) => {
        if (!isNaN(conversion)) {
          formActions.calculateResult(conversion);
          currencyActions.addHistoryData(formData);
          if (!historyOpened) {
            history.push('/history');
          }
        }
      });
  };

  return (
    <LoadingWrapper isLoading={isApiInProgress}>
      <Converter
        convert={handleConvertAction}
        currencies={currencies}
        historyClear={currencyActions.clearHistoryData}
        data={formData}
        historyData={historyData}
        setData={(t: FormChangeEnum, v: any) => formActions.setFormValue(t, v)}
        switchDirection={formActions.switchConversionDirection}
        isHistoryOpened={historyOpened}
      />
    </LoadingWrapper>
  );
}

export default App;
