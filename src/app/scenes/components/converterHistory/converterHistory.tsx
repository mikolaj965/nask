import './converterHistory.scss';

import React, { FC } from 'react';

import { IConversionHistoryModel } from '../../../models/conversionHistory.model';
import { Link } from 'react-router-dom';

interface IConverterHistoryProps {
    clear: () => void;
    data: IConversionHistoryModel[];
    opened: boolean;
}

const ConverterHistory: FC<IConverterHistoryProps> = props => {
    const { clear, data, opened } = props;

    const handleHistoryClear = () => clear();

    return (
        <div className="converter-history-container">
            {opened && (
                <div className="converter-history-body">
                    <div className="history-table-header">
                        <div className="history-table-cell date">Data</div>
                        <div className="history-table-cell before">Przed konwersją</div>
                        <div className="history-table-cell arrow"></div>
                        <div className="history-table-cell after">Po konwersji</div>
                    </div>
                    <div style={{ flex: 2, overflowY: 'auto' }}>
                        {data && data.map(row => (
                            <div className="history-table-row" key={row.id}>
                                <div className="history-table-cell date">{row.date.format('DD.MM.YYYY')}</div>
                                <div className="history-table-cell before">
                                    {row.before.value ? row.before.value.toFixed(2) : row.before.value} {row.before.type}
                                </div>
                                <div className="history-table-cell arrow">🡒</div>
                                <div className="history-table-cell after">
                                    {row.after.value ? row.after.value.toFixed(2) : row.after.value} {row.after.type}
                                </div>
                            </div>
                        ))}
                    </div>
                    {data && data.length !== 0 && (
                        <div id="clear-history" onClick={handleHistoryClear}>Wyczyść historię</div>
                    )}
                </div>
            )}
            <div className="converter-history-head">
                <Link to={opened ? '/' : '/history'} >
                    {opened && (<>x </>)}
                    Historia
                </Link>
            </div>
        </div>
    );
}

export default ConverterHistory;
