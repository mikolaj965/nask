import './button.scss';

import React, { FC } from 'react';

interface IButtonProps {
    onClick: () => void;
    id: string;
    text: string;
    error?: boolean;
}

const Button: FC<IButtonProps> = props => {
    const { onClick, id, text, error } = props;

    const handleButtonClick = () => onClick();

    return (
        <button
            id={id}
            onClick={error ? undefined : handleButtonClick}
            className={error ? "button-inactive" : "button-active"}
        >
            {text}
        </button>
    );
}

export default Button;
