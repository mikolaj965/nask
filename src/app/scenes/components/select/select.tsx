import './select.scss';

import React, { FC } from 'react';

interface ISelectProps {
    className?: string;
    id: string;
    onChange: (v: string | number) => void;
    options: (string | number)[];
    value: string | number;
}

const Select: FC<ISelectProps> = props => {
    const { className, id, onChange, options, value } = props;

    const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => onChange(e.target.value);

    return (
        <div style={{ position: 'relative' }}>
            <select value={value} className={className} onChange={handleSelectChange}>
                {options?.map((o: string | number) => <option key={`${id}_${o}`}>{o}</option>)}
            </select>
        </div>
    );
}

export default Select;
