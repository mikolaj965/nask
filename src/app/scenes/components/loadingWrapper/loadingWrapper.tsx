import './loadingWrapper.scss';

import React, { FC } from 'react';

interface ILoadingWrapperProps {
    isLoading: boolean;
}

const LoadingWrapper: FC<ILoadingWrapperProps> = props => (
    <>
        {props.children}
        {props.isLoading && <div className="loadingOverlay">
            <div className="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>}
    </>
);

export default LoadingWrapper;
