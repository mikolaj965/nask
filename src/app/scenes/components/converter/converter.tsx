import './converter.scss';

import React, { FC, useMemo } from 'react';

import Button from '../button/button';
import ConverterHistory from '../converterHistory/converterHistory';
import { FormChangeEnum } from '../../../../state/form/form-utils';
import { IConversionHistoryModel } from '../../../models/conversionHistory.model';
import { IConverterFormModel } from '../../../models/converterForm.model';
import { ICurrencyDetailsAPIModel } from '../../../models/currencyDetailsAPI.model';
import Input from '../input/input';
import Select from '../select/select';
import { SwapSvg } from './swap-svg';

interface IConverterProps {
    currencies: ICurrencyDetailsAPIModel[];
    data: IConverterFormModel;
    convert: () => void;
    historyClear: () => void;
    historyData: IConversionHistoryModel[];
    setData: (type: FormChangeEnum, value: any) => void;
    switchDirection: () => void;
    isHistoryOpened: boolean;
}

const Converter: FC<IConverterProps> = props => {
    const { convert, currencies, data, isHistoryOpened, historyData, historyClear, setData, switchDirection } = props;

    // ACTIONS
    const handleFormChange = (t: FormChangeEnum) => (v: any) => setData(t, v);
    const handleConvertClick = () => convert();
    const handleSwitchDirectionChange = () => switchDirection();

    const currencyNames = useMemo(() => currencies.map(c => c.id), [currencies]);
    const isFromInputValid = useMemo(() => data.from.value && !isNaN(Number(data.from.value)), [data.from.value]);
    const isConverionLabelValid = useMemo(() => data.from.type && data.to.type, [data.from.type, data.to.type]);

    return (
        <div className="converter-container" style={{ width: isHistoryOpened ? '700px' : '400px' }}>
            <div className="converter-form">
                <h2>Konwerter walut</h2>
                <div className="full-row">
                    <Input
                        append={data.from.type ? data.from.type : "---"}
                        value={data.from.value}
                        placeholder="Wpisz kwotę"
                        onChange={handleFormChange(FormChangeEnum.MONEY_FROM)}
                        className="converter-input"
                        error={!isFromInputValid}
                    />
                </div>

                <div className="full-row">
                    <Input
                        append={data.to.type ? data.to.type : "---"}
                        value={data.to.value}
                        placeholder="Wynik"
                        disabled
                        onChange={() => { }}
                        className="converter-input"
                    />
                </div>

                <div className="full-row" style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Select
                        id="CURRENCY_FROM"
                        options={currencyNames}
                        value={data.from.type}
                        onChange={handleFormChange(FormChangeEnum.CURRENCY_FROM)}
                        className="converter-select"
                    />
                    <div className="converter-swap-currency" onClick={handleSwitchDirectionChange}>
                        <SwapSvg />
                    </div>
                    <Select
                        id="CURRENCY_TO"
                        options={currencyNames}
                        value={data.to.type}
                        onChange={handleFormChange(FormChangeEnum.CURRENCY_TO)}
                        className="converter-select"
                    />
                </div>

                <div className="full-row">
                    <Button
                        onClick={handleConvertClick}
                        id="convert-button"
                        error={!isFromInputValid || !isConverionLabelValid}
                        text="Konwertuj"
                    />
                </div>
            </div>
            <div className="converter-radius"><div>&nbsp;</div></div>
            <ConverterHistory
                clear={historyClear}
                data={historyData}
                opened={isHistoryOpened}
            />
        </div>
    );
}

export default Converter;
