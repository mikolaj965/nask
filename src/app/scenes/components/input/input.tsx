import './input.scss';

import React, { FC } from 'react';

import classNames from 'classnames';

interface IInputProps {
    append?: string;
    onChange: (v: string | number) => void;
    disabled?: boolean;
    placeholder?: string;
    className?: string;
    error?: boolean;
    value: string | number | null;
}

const Input: FC<IInputProps> = props => {
    const { append, disabled, onChange, className, placeholder, error, value } = props;

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => onChange(e.target.value);

    return (
        <div style={{ position: 'relative' }}>
            <input
                value={value !== null ? value : ""}
                onChange={handleInputChange}
                placeholder={placeholder}
                className={classNames(className, {
                    'input-error': error
                })}
                disabled={disabled}
            />
            {append && (
                <span className={classNames('input-append', {
                    'input-error': error
                })}>
                    {append}
                </span>
            )}
        </div>
    );
}

export default Input;
