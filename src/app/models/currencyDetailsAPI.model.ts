export interface ICurrencyDetailsAPIModel {
    currencyName: string;
    currencySymbol?: string;
    id: string;
}

export const defaultCurrencyDetailsAPIModel: ICurrencyDetailsAPIModel = {
    currencyName: "Polish Zloty",
    currencySymbol: "zł",
    id: "PLN"
};

