import { IConverterFormCurrencyModel, defaultConverterFormCurrencyModel } from './converterFormCurrency.model';

export interface IConverterFormModel {
    from: IConverterFormCurrencyModel,
    to: IConverterFormCurrencyModel
}

export const defaultConverterFormModel: IConverterFormModel = {
    from: defaultConverterFormCurrencyModel,
    to: defaultConverterFormCurrencyModel
};