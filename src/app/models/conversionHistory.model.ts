import { IConverterFormCurrencyModel, defaultConverterFormCurrencyModel } from './converterFormCurrency.model';
import moment, { Moment } from 'moment';

export interface IConversionHistoryModel {
    id: string;
    date: Moment;
    before: IConverterFormCurrencyModel;
    after: IConverterFormCurrencyModel;
}

export const defaultConverterFormModel: IConversionHistoryModel = {
    id: "",
    date: moment(),
    before: defaultConverterFormCurrencyModel,
    after: defaultConverterFormCurrencyModel
};
