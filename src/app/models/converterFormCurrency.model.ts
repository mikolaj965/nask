export interface IConverterFormCurrencyModel {
    value: number | null;
    type: string;
}

export const defaultConverterFormCurrencyModel: IConverterFormCurrencyModel = {
    value: 0,
    type: 'PLN'
};