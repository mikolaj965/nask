import 'react-toastify/dist/ReactToastify.css';
import './index.css';

import * as serviceWorker from './serviceWorker';

import { Route, HashRouter as Router, Switch } from 'react-router-dom';

import App from './app/app';
import React from 'react';
import ReactDOM from 'react-dom';
import { ToastContainer } from 'react-toastify';
import axios from 'axios';
import config from './axios.interceptor.config';

axios.interceptors.response.use(config.onRequestFulfilled, config.onRequestRejected);

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/">
          <App historyOpened={false} />
        </Route>
        <Route path="/history">
          <App historyOpened />
        </Route>
      </Switch>
    </Router>
    <ToastContainer position="bottom-right" />
  </React.StrictMode>,
  document.getElementById('root')
);
serviceWorker.unregister();
