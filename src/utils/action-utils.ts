export const SUCCESS = (type: string) => `${type}_SUCCESS`;

export const ERROR = (type: string) => `${type}_ERROR`;

export const PENDING = (type: string) => `${type}_PENDING`; 
